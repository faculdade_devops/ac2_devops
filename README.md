# Como usei
O ```docker run dockerimagem``` não funcionou, então tive que usar ```docker run -p 7000:4000 dockerimagem``` para conseguir ver a execução do da aplicação Python e entrar na porta 7000.

# Integrantes do grupo 
* Sergio Caique da Silva 
    * RA: 2102688
* André Luiz Costa Coelho 
    * RA: 2202282
* Ivan Autuono de Souza 
    * RA: 2202353
* Nicholas Gabriel Pires Dias 
    * RA: 2202411
* Gabriel Cézar do Nascimento 
    * RA: 2202762
* Celso da Silva ferreira 
    * RA: 2202827